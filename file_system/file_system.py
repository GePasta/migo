
class FileSystem:

    def __init__(self, context):
        self.context = context

    def add_file(self, file_name):
        print(f"File {file_name} added")
        pass  # calls the AddFile

    def add_dir(self, dir_name):
        print(f"File {dir_name} added")
        pass  # calls the AddDir

    def change_dir(self, path):
        print(f"Changed path to {path}")
        pass  # calls the ChangeDir

    def dir_up(self):
        print("cd ..")
        pass  # calls the DirUp

    def pwd(self):
        print("Printing working director")
        return "/"  # calls the Pwd

    def remove_file(self, file_name):
        print(f"{file_name} has been removed")
        pass  # calls the rm function

    def remove_dir(self, dir_name):
        print(f"{dir_name} has been removed")
        pass  # calls the rmdir  function

    def ls(self, *args):
        print("Printing available files")
        return "readme.txt bin"  # calls the ls function
