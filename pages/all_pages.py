from pages import DirectoryPage, FilePage, NavigationPage
from file_system.file_system import FileSystem


class AllPages:
    directory_page: DirectoryPage
    file_page: FilePage
    navigation_page: NavigationPage
    file_system: FileSystem

    def __init__(self, context, tags):
        self.file_system = FileSystem(context)
        if "directory" in tags:
            self.directory_page = DirectoryPage(context)
        if "file" in tags:
            self.file_page = FilePage(context)
        if "navigation" in tags:
            self.navigation_page = NavigationPage(context)
