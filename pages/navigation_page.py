from file_system.file_system import FileSystem


class NavigationPage(FileSystem):

    def navigate_to_default_directory(self):
        current_dir = self.pwd().strip("/")
        if current_dir:
            return
        else:
            self.change_dir("/")
