from file_system.file_system import FileSystem


class FilePage(FileSystem):
    def delete_file(self, path, file_name):
        if self.pwd() != path:
            self.change_dir(path)
        self.remove_file(file_name)

