from file_system.file_system import FileSystem


class DirectoryPage(FileSystem):
    def delete_dir(self, path, dir_name):
        if self.pwd() != path:
            self.change_dir(path)
        self.remove_dir(dir_name)
