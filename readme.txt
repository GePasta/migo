Done:
- File structure,
- Tags in tests for same performance of the various methods remains consistent across code update,
- Environment with dynamic creation of pages for specifit test case,
- Feature files with titles of test cases,
- Decorators checking if system is set up for test,
- Teardown,
- TC testing add_file method,
- Part of TC testing add_dir method,
- Typing.

TODO:
- Write rest of the test cases,
- Mock application,
- Write docstrings for methods,
- Write clean up for deleting not empty directory,
- Create load tests,
- Create test cases with multithreading with locks 
