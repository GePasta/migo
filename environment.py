from typing.context_type import Context
from pages.all_pages import AllPages
from objects.generic_object import GenericObject


def before_scenario(context: Context, scenario):
    context.pages = AllPages(context, scenario.tags)
    context.test_data = GenericObject()
