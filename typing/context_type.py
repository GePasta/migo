import behave.runner

from pages import AllPages
from objects.generic_object import GenericObject


class Context(behave.runner.Context):
    pages: AllPages
    test_data: GenericObject
