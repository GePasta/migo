def check_if_file_does_not_exist(func):
    def wrapper(*args):
        context = args[0]
        row = context.table.rows[0]
        dir_list = context.pages.file_system.ls("-p | grep -v /")
        if row["file_name"] in dir_list:
            context.pages.file_system.remove_file(row["file_name"])
        func(*args)
    return wrapper
