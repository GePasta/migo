def check_if_dir_does_not_exist(func):
    def wrapper(*args):
        context = args[0]
        row = context.table.rows[0]
        dir_list = context.pages.file_system.ls("-d */")
        if row["dir_name"] in dir_list:
            context.pages.file_system.remove_dir(row["dir_name"])
        func(*args)
    return wrapper
