Feature: Add two new file with different characters

  @file @navigation
  Scenario Outline: Add two new file with different characters
    Given the user is in the root directory
    When the user creates two files with the same name
      | file_name   |
      | <file_name> |
    Then only one new file should exists
      | file_name   |
      | <file_name> |

    Examples:
      | file_name  |
      | te/st       |
      | TE>ST       |
      | tes:t1234   |
      | 1234&test   |