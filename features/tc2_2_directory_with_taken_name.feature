Feature: Add new directory with taken name

  @directory @navigation
  Scenario Outline: Add new directory with taken name
    Given the user is in the root directory
    And the directory is created
      | dir_name    |
      | <dir_name> |
    When the user creates a new directory
      | dir_name   |
      | <dir_name> |
    Then the new directory should not exists
      | dir_name   |
      | <dir_name> |

    Examples:
      | dir_name |
      | test  |