Feature: Add two new file with same naming

  @file @navigation
  Scenario Outline: Add two new file with same naming
    Given the user is in the root directory
    When the user creates two files with the same name
      | file_name   |
      | <file_name> |
    Then only one new file should exists
      | file_name   |
      | <file_name> |

    Examples:
      | file_name         |
      | test.txt test.txt |