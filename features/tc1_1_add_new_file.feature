Feature: Add new file

  @file @navigation
  Scenario Outline: Add new file
    Given the user is in the root directory
    When the user creates a new file
      | file_name   |
      | <file_name> |
    Then the new file exists
      | file_name   |
      | <file_name> |

    Examples:
      | file_name |
      | test.txt  |