Feature: Add new file with same name as directory

  @file @navigation @directory
  Scenario Outline: Add new file with same name as directory
    Given the user is in the root directory
    And the directory is created
      | dir_name    |
      | <file_name> |
    When the user creates a new file
      | file_name   |
      | <file_name> |
    Then the new file should not exists
      | file_name   |
      | <file_name> |

    Examples:
      | file_name |
      | test      |