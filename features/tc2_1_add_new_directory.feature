Feature: Add new directory

  @directory @navigation
  Scenario Outline: Add new directory
    Given the user is in the root directory
    When the user creates a new directory
      | dir_name   |
      | <dir_name> |
    Then the new directory exists
      | dir_name   |
      | <dir_name> |

    Examples:
      | dir_name |
      | test  |