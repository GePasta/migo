Feature: Add two new file with different characters

  @file @navigation
  Scenario Outline: Add two new file with different characters
    Given the user is in the root directory
    When the user creates a new file
      | file_name   |
      | <file_name> |
    Then the new file exists
      | file_name   |
      | <file_name> |

    Examples:
      | file_name  |
      | test       |
      | TEST       |
      | test1234   |
      | 1234test   |
      | T@#$%^^&   |
      | TESST_test |