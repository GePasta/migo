from behave import when

from decorators.dir_decorators import check_if_dir_does_not_exist
from decorators.file_decorators import check_if_file_does_not_exist
from typing.context_type import Context


@when('the user creates two files with the same name')
@when('the user creates a new file')
@check_if_file_does_not_exist
def step_create_file(context: Context):
    row = context.table.rows[0]
    current_dir = context.pages.file_system.pwd()
    try:
        context.pages.file_system.add_file(row["file_name"])
    except (IOError, OSError) as ex:
        print(f"Unable to create file: {ex}")
        context.test_data.exception = True
    if not hasattr(context.test_data, 'exception'):
        context.add_cleanup(context.pages.file_page.delete_file, current_dir, row["file_name"].split()[0])


@when('the user creates a new directory')
@check_if_dir_does_not_exist
def step_create_dir(context: Context):
    row = context.table.rows[0]
    current_dir = context.pages.file_system.pwd()
    context.test_data.dir_list = context.pages.file_system.ls()
    try:
        context.pages.file_system.add_dir(row["dir_name"])
    except (IOError, OSError) as ex:
        print(f"Unable to create directory: {ex}")
        context.test_data.exception = True
    if not hasattr(context.test_data, 'exception'):
        context.add_cleanup(context.pages.directory_page.delete_dir, current_dir, row["dir_name"].split()[0])





