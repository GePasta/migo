from behave import then
from grappa import should

from typing.context_type import Context


@then('the new file exists')
def step_check_file(context: Context):
    row = context.table.rows[0]
    context.pages.file_page.ls() | should.contain(row['file_name'])


@then('only one new file should exists')
def step_check_file(context: Context):
    row = context.table.rows[0]
    file_name = set(row['file_name'].split())
    context.pages.file_page.ls() | should.contain(row['file_name'])
    context.pages.file_page.ls().count(file_name) | should.be.equal.to(1)


@then('the new file should not exists')
def step_check_file(context: Context):
    row = context.table.rows[0]
    context.pages.file_page.ls("-p | grep -v /") | should.do_not.contain(row['file_name'])


@then('the new directory exists')
def step_check_dir(context: Context):
    row = context.table.rows[0]
    context.pages.file_system.ls("-d */") | should.do_not.contain(row['dir_name'])


@then('the new directory should not exists')
def step_check_dir_not_present(context: Context):
    row = context.table.rows[0]
    context.test_data.dir_list | should.be.equal.to(context.pages.file_system.ls()) # noqa
    context.test_data.dir_list.count(row['dir_name']) | should.be.equal.to(context.pages.file_system.ls().  # noqa
                                                                           count(row['dir_name']))
