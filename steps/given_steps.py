from behave import given

from typing.context_type import Context


@given('the user is in the root directory')
def step_navigate_to_root_dir(context: Context):
    context.pages.navigation_page.navigate_to_default_directory()


@given('the directory is created')
def step_navigate_to_root_dir(context: Context):
    row = context.table.rows[0]
    context.pages.directory_page.add_dir(row["dir_name"])
